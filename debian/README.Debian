cacti for Debian 

* Default initial username/password

  The default username is admin. The initial password for the admin user in
  Debian for Cacti is taken from the connection string for the MySQL database
  as found in /etc/cacti/debian.php in the line containing
  $database_password. Of course you are fully free to change the password of
  course (via the cacti interface).

* Manual installation

  Note that cacti uses automatic configuration via dbconfig-common.
  However, if you've chosen to go the manual route, the following
  information may be helpful.

  Using the username and password you provided in debconf (and stored
  in the cacti config file /etc/cacti/debian.php), create a database
  and load up the cacti skeleton. Assuming you chose a database and
  user both named 'cacti' and your database is on localhost:

  mysql -u root -p -e "create database cacti"
  mysql -u root -p -e "create user 'cacti'@'localhost';"
  mysql -u root -p -e "alter user 'cacti'@'localhost' identified by 'yourpasswordhere';"
  mysql -u root -p -e "grant all privileges on cacti.* to 'cacti'@'localhost';"
  mysql -u root -p -e "flush privileges;"

  cat /usr/share/doc/cacti/cacti.sql | mysql -u cacti -p cacti

  Next, go to http://$yourhost/cacti/, and follow the on-screen directions.
  The default login/password is admin/admin.

* Custom local scripts

  If you've written your own scripts for monitoring various things in cacti, it
  is strongly recommended that you not place them under /usr/share, as package
  maintainers will sometimes wrongly assume that we have total control over the
  filespace there. Instead, you should consider using an alternate location,
  such as somewhere under /usr/local/share/cacti, which should be already
  created for you when you install this package.

* Cacti doesn't gather any data???

  Here's a collection of tips observations that might help if you can't
  figure out why cacti isn't gathering any data (assuming you're using
  the standard poller and not spine).

  Cacti's poller is a pure php script located at

  	/usr/share/cacti/site/poller.php

  Because it's normally a rather noisy script, the stderr is redirected
  to an error logfile in

  	/var/log/cacti/poller-error.log

  It's also possible something may be in

  	/var/log/cacti/cacti.log

  It's also been reported that php-cli requires safe mode to be turned off.
  if you get errors like:

  	sh: line 1: /php: No such file or directory
  	sh: line 1: /rrdtool: No such file or directory

  then try disabling safe mode for your php-cli in /etc/php/*/cli/php.ini.
  (See bug http://bugs.debian.org/364897) 
 
* Time zone names

  Cacti takes the names of time zones from the mysql.time_zone_name
  table. Because this table is a global table, which on Debian is empty by
  default, I don't want to manipulate it during install of cacti. If you want
  cacti to benefit from time zone names, than you should run the following
  command manually.

  $ mysql_tzinfo_to_sql /usr/share/zoneinfo | sudo mysql mysql

 -- Paul Gevers <elbrus@debian.org>, Sat, 10 Jun 2017 20:45:50 +0200
