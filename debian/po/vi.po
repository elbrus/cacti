# Vietnamese translation for cacti.
# Copyright © 2005 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: cacti 0.8.6d-1\n"
"Report-Msgid-Bugs-To: cacti@packages.debian.org\n"
"POT-Creation-Date: 2012-05-17 21:33+0200\n"
"PO-Revision-Date: 2005-06-12 20:42+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <gnomevi-list@lists.sourceforge.net>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"

#. Type: select
#. Choices
#: ../cacti.templates:2001
msgid "None"
msgstr ""

#. Type: select
#. Description
#: ../cacti.templates:2002
#, fuzzy
#| msgid "Webserver type"
msgid "Web server:"
msgstr "Loại trình phục vụ Mạng"

#. Type: select
#. Description
#: ../cacti.templates:2002
msgid ""
"Please select the web server for which Cacti should be automatically "
"configured."
msgstr ""

#. Type: select
#. Description
#: ../cacti.templates:2002
#, fuzzy
#| msgid ""
#| "Select \"None\" if you would like to configure your webserver by hand."
msgid "Select \"None\" if you would like to configure the web server manually."
msgstr "Hãy chọn «Không có» nếu bạn muốn tự cấu hình trình phục vụ Mạng."

#~ msgid "Which kind of web server should be used by cacti?"
#~ msgstr "Trình cacti sẽ dùng trình phục vụ Mạng loại nào?"

#~ msgid "Apache, Apache-SSL, Apache2, All, None"
#~ msgstr "Apache, Apache-SSL, Apache2, Tất cả, Không có"

#~ msgid "MySQL installs and upgrades must be performed manually!"
#~ msgstr "• Cần phải tự cài đặt và nâng cập MySQL. •"

#~ msgid ""
#~ "For the time being, support for automatic upgrading of the cacti mysql "
#~ "database has been disabled.  Please see the rationale and what you will "
#~ "need to do for either installing or upgrading cacti in /usr/share/doc/"
#~ "cacti/README.Debian.gz."
#~ msgstr ""
#~ "Hiện thời, tính năng tự động nâng cấp cơ sở dữ liệu MySQL cacti bị tắt. "
#~ "Hãy xem lý do căn bản, và phương pháp cài đặt hay nâng cấp trình cacti, "
#~ "trong tập tin «/usr/share/doc/cacti/README.Debian.gz»."

#~ msgid ""
#~ "Note that you will still be prompted for the relevant information, which "
#~ "will be used to generate the appropriate configuration files."
#~ msgstr ""
#~ "Hãy ghi chú: trình này sẽ còn nhắc bạn nhập thông tin liên quan, mà sẽ "
#~ "được dùng để tạo ra những tập tin cấu hình thích hợp."

#~ msgid "Database structure changes in cacti configuration"
#~ msgstr "Thay đổi cấu trúc cơ sở dữ liệu trong cấu hình cacti"

#~ msgid ""
#~ "You are upgrading from a pre 0.8.x version.  Unfortunately, your old "
#~ "database will not work with the latest version of cacti.  Please see /usr/"
#~ "share/doc/cacti/README.Debian.gz to see what your options are."
#~ msgstr ""
#~ "Lúc này bạn nâng cấp từ một phiên bản trước 0.8.x. Tiếc là cơ sở dữ liệu "
#~ "cũ của bạn sẽ không hoạt động với phiên bản cacti mới nhất. Hãy xem tập "
#~ "tin «/usr/share/doc/cacti/README.Debian.gz» để tìm tùy chọn."

#~ msgid "MySQL server host name"
#~ msgstr "Tên máy của máy phục vụ MySQL"

#~ msgid "Please enter the name of the machine hosting the cacti database."
#~ msgstr "Hãy nhập tên máy của máy hỗ trợ cơ sở dữ liệu cacti."

#~ msgid "Database name for cacti"
#~ msgstr "Tên cơ sở dữ liệu cho cacti"

#~ msgid ""
#~ "Please enter the cacti database name. Cacti will store and fetch data "
#~ "there."
#~ msgstr ""
#~ "Hãy nhập tên cơ sở dữ liệu cacti. Trình cacti sẽ lưu dữ liệu vào đó, và "
#~ "gọi dữ liệu từ đó."

#~ msgid "MySQL administrator username"
#~ msgstr "Tên người dùng của quản trị MySQL"

#~ msgid ""
#~ "Please enter the MySQL administrator name (needed for cacti database "
#~ "creation)."
#~ msgstr "Hãy nhập tên quản trị MySQL (cần thiết để tạo cơ sở dữ liệu cacti)."

#~ msgid "MySQL administrator password"
#~ msgstr "Mật khẩu quản trị MySQL"

#~ msgid "Enter \"none\" if there is no password for MySQL administration."
#~ msgstr ""
#~ "Hãy nhập «none» (không có) nếu không dùng mật khẩu để quản lý MySQL."

#~ msgid "Cacti database username"
#~ msgstr "Tên người dùng cơ sở dữ liệu cacti"

#~ msgid ""
#~ "Please enter the name which will be used for connecting to the cacti "
#~ "database."
#~ msgstr "Hãy nhập tên sẽ được dùng để kết nối đến cơ sở dữ liệu cacti."

#~ msgid "Cacti user password"
#~ msgstr "Mật khẩu người dùng cacti"

#~ msgid ""
#~ "Please enter a password for the Cacti database user (default is \"cacti"
#~ "\")."
#~ msgstr ""
#~ "Hãy nhập một mật khẩu cho người dùng cơ sở dữ liệu cacti (mặc định là "
#~ "«cacti»)."

#~ msgid "Do you want to purge the database when purging the package?"
#~ msgstr "Bạn có muốn xóa bỏ cơ sở dữ liệu khi tẩy gói tin này không?"

#~ msgid ""
#~ "Accept here if you want to drop the database and the corresponding user "
#~ "when purging the package."
#~ msgstr ""
#~ "Hãy chấp nhận tùy chọn này, nếu bạn muốn xóa bỏ cơ sở dữ liệu và người "
#~ "dùng tương ứng, khi tẩy gói tin này."

#~ msgid "MySQL administrator password confirmation"
#~ msgstr "Xác nhận mật khẩu quản trị MySQL"

#~ msgid "Please confirm the password for the MySQL administrator."
#~ msgstr "Hãy xác nhận mật khẩu cho quản trị MySQL."

#~ msgid "Error"
#~ msgstr "Lỗi"

#~ msgid ""
#~ "The password and its confirmation do not match. Please re-enter the "
#~ "password!"
#~ msgstr "Hai mật khẩu không khớp được. Hãy nhập lại."

#~ msgid "Store the MySQL admin password"
#~ msgstr "Lưu mật khẩu quản trị MySQL"

#~ msgid ""
#~ "To update the cacti database automaticlly during a package upgrade the "
#~ "MySQL admin password must be safed in the debconf database. This is a "
#~ "security risk!!!"
#~ msgstr ""
#~ "Để tự động cập nhật cơ sở dữ liệu cacti trong khi nâng cấp gói tin, cần "
#~ "phải lưu mật khẩu quản trị MySQL vào cơ sở dữ liệu debconf. • Làm như thế "
#~ "rủi ro bảo mật. •"

#~ msgid ""
#~ "If the password is not stored in the database it will asked everytime!"
#~ msgstr ""
#~ "Tuy nhiên, nếu mật khẩu ấy không được lưu trong cơ sở dữ liệu, trình cấu "
#~ "hình sẽ nhắc bạn nhập nó mỗi lúc cần thiết."
