#!/bin/sh

set -e

# source debconf stuff
. /usr/share/debconf/confmodule
db_version 2.0
# source dbconfig-common stuff
. /usr/share/dbconfig-common/dpkg/postinst.mysql
dbc_first_version="0.8.6g-3"
dbc_generate_include="template:/etc/cacti/debian.php"
dbc_generate_include_owner="root:www-data"
dbc_generate_include_perms="0640"
dbc_generate_include_args="--dbname=database_default --dbpass=database_password --dbuser=database_username --dbserver=database_hostname --dbport=database_port -o template_infile=/usr/share/cacti/conf_templates/debian.php.dist"
dbc_mysql_createdb_encoding="latin1"
dbc_sql_substitutions="true"
dbc_go cacti $@
ucfr cacti /etc/cacti/debian.php

#
# Skip, if we are not in "configure" or "reconfigure" state
#
if [ "$1" = "configure" ] || [ "$1" = "reconfigure" ]; then

    #
    # retrieve various configuration options from debconf
    #
    db_get cacti/webserver
    WWWTYPE="$RET"

    ucf --debconf-ok /usr/share/cacti/conf_templates/cacti.apache.conf /etc/apache2/conf-available/cacti.conf
    ucfr cacti /etc/apache2/conf-available/cacti.conf
    ucf --debconf-ok /usr/share/cacti/conf_templates/cacti.lighttpd.conf /etc/lighttpd/conf-available/20-cacti.conf
    ucfr cacti /etc/lighttpd/conf-available/20-cacti.conf

    # done with debconf...
    db_stop

    # Only create common files (logfile) and grant permissions on first install
    if [ ! -n "$2" ] ; then
        touch /var/log/cacti/rrd.log /var/log/cacti/cacti.log /var/log/cacti/poller-error.log
        chmod 0640 /var/log/cacti/rrd.log /var/log/cacti/cacti.log /var/log/cacti/poller-error.log
        chown www-data:www-data /var/log/cacti/ /var/log/cacti/cacti.log

        # Check if
        # - the admin wanted dbconfig to help
        # - there is a password to use
        # - if the dbconfig installation went ok (e.g. no noninteractive fail)
        #   see also bug 822945
        if [ "$dbc_install" = "true" ] && [ -n "$dbc_dbpass" ] && [ "${_dbc_on_error_option}" != "ignore" ] ; then
            # An upstream clean install has a well-known admin password and
            # triggers a change before one can continue. However, on Debian we
            # already have a good password that we can communicate to the admin, so
            # make the setup safe from the start.
            dbc_mysql_exec_command "UPDATE user_auth SET password=md5('$dbc_dbpass'), must_change_password='' WHERE id=1"

            # An upstream clean install triggers some questions before you can use
            # Cacti for the first time. However, on Debian that should already be
            # alright. Prevent triggering these questions by filling the latest
            # version number into the database.
            cacti_upstream_ver=$(dpkg-query -f '${Version}' -W 'cacti' | sed -e s/\+ds[0-9]-.*// -e s/~beta[0-9]*//)
            dbc_mysql_exec_command "UPDATE version SET cacti='$cacti_upstream_ver'"

            # Until now (2016-04-01) the versions of rrdtool in Cacti are always of
            # the form rrd-[0-9].[0-9].x, so let's assume that.
            rrd_ver=$(dpkg-query -f '${Version}' -W 'rrdtool')
            dbc_mysql_exec_command "INSERT INTO settings SET name='rrdtool_version', value='rrd-${rrd_ver%%[0-9]-*}x'"

            # Since cacti version 1.0.0, by default there aren't any templates
            # loaded anymore which leaves an empty experience. More so because
            # there is no web interface that tells you of the existence, nor is
            # able to load the "packages". So let's load the default ones here.
            #
            # Because we want to keep the debsums checksums constant, we don't
            # actually want to change the scripts (they are not equal, see
            # https://github.com/Cacti/cacti/issues/810). So, make sure we run
            # these commands as the www-data user, because that can execute the
            # MySQL calls, but can't change the files (and that is OK).
            echo -n "Loading default cacti templates (may take some time)."
	        for template in /usr/share/cacti/site/install/templates/*.xml.gz ; do
                su --shell /bin/sh www-data -c "/usr/share/cacti/cli/import_package.php \
            --filename=${template}" > /dev/null
                echo -n "."
	        done
            echo "  done."

            # And since we don't pass through the install script anymore, we need
            # to rebuild the poller cache now (but suppress normal output as it
            # looks scary during install).
            php /usr/share/cacti/cli/rebuild_poller_cache.php > /dev/null
        fi
    fi

    # Make sure the database is up-to-date
    if [ -n "$2" ] ; then
        { echo "Running cli/upgrade_database.php as part of package update..." ; \
          php /usr/share/cacti/cli/upgrade_database.php ; } | \
            if [ -f /usr/bin/ts ] ; then LC_TIME=C ts "%m/%d/%Y %I:%M:%S %p" | tee -a /var/log/cacti/cacti.log ; \
            else tee -a /var/log/cacti/cacti.log ; fi
    fi

    # Update the webserver, if needed
    case $WWWTYPE in
        apache2)
            if [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
                . /usr/share/apache2/apache2-maintscript-helper
                apache2_invoke enconf cacti
            else
                echo "Apache2 not installed, skipping"
            fi
            ;;
        lighttpd)
            if [ -e /etc/lighttpd/conf-available/20-cacti.conf ] ; then
                if which lighty-enable-mod >/dev/null 2>&1 ; then
                    lighty-enable-mod cacti || \
                        if [ "$?" != "2" ] ; then exit 1 ; else true ; fi
                else
                    echo "Lighttpd not installed, skipping"
                fi
            fi
            ;;
        *)
            ;;
    esac

    # Always trigger reload of webservers if the configuration is enabled
    if [ -e /etc/lighttpd/conf-enabled/20-cacti.conf ] ; then
        #   We need to take care: bug #446324
        invoke-rc.d lighttpd reload 3>/dev/null || true
    fi

fi

#DEBHELPER#
exit 0
